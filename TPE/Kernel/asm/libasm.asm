GLOBAL cpuVendor
GLOBAL syscaller
GLOBAL outb
GLOBAL outd
GLOBAL outq
GLOBAL inb
GLOBAL ind
GLOBAL inq
GLOBAL getRax
GLOBAL int80handler
GLOBAL inpw
GLOBAL outpw

section .text
	
cpuVendor:
	push rbp
	mov rbp, rsp

	push rbx

	mov rax, 0
	cpuid


	mov [rdi], ebx
	mov [rdi + 4], edx
	mov [rdi + 8], ecx

	mov byte [rdi+13], 0

	mov rax, rdi

	pop rbx

	mov rsp, rbp
	pop rbp
	ret

syscaller: 
	mov rax, rdi
	int 80h
	ret


getRax:
	ret


; void outb(uint16_t port, uint8_t value)
outb:
	mov rdx, rdi
	mov rax, rsi
	out dx, al
	ret

; void outd(uint16_t port, uint16_t value)
outd:
	mov rdx, rdi
	mov rax, rsi
	out dx, ax
	ret

; void outq(uint16_t port, uint32_t value)
outq:
	mov rdx, rdi
	mov rax, rsi
	out dx, eax
	ret

; uint8_t inb(uint16_t port)
inb:
	mov rdx, rdi
	in al, dx
	ret

; uint16_t ind(uint16_t port)
ind:
	mov rbx, rdi
	in ax, dx
	ret

; uint32_t inq(uint16_t port)
inq:
	mov rbx, rdi
	in eax, dx
	ret
; out for video function
outpw:
	mov rdx, rdi
	out dx, ax
	ret
; in for video function
inpw:
	mov rdx, rdi
	in ax, dx
	ret

