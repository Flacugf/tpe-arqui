    #include <stdint.h>
    #include <naiveConsole.h>

void outb(uint16_t port, uint8_t value);
void outd(uint16_t port, uint16_t value);
void outq(uint16_t port, uint32_t value);
uint8_t inb(uint16_t port);
uint16_t ind(uint16_t port);
uint32_t inq(uint16_t port);
void syscaller(uint16_t interrupt);
char getRax();

#define IOADDRESS 0xC000
#define BUFFER 0x550000

typedef struct rtlStruct{

    uint8_t macAddress[6];
    uint8_t *rxBuffer;
    uint8_t *txBuffer;
    uint8_t rxNum;
    uint8_t txNum;

} rtlStruct;

static rtlStruct rtl;

void turnOnRTL(){
    outb(IOADDRESS + 0x52, 0x0);
}

void softwareReset(){
    outb(IOADDRESS + 0x37, 0x10);
    while( (inb(IOADDRESS + 0x37) & 0x10) != 0) { }
}

void initReceivedBuffer(){
    rtl.rxBuffer = (unsigned char*) BUFFER;
    outd(IOADDRESS + 0x30, (uintptr_t)rtl.rxBuffer);
}

void setImrIsr(){
    outd(IOADDRESS + 0x3C, 0x0005);
}

void configureReceiveBuffer(){
    outq(IOADDRESS + 0x44, 0xf | (1 << 7));
}

void enableReceiveAndTransmitter(){
    outb(IOADDRESS + 0x37, 0x0C);
}

void isrHandler(){
    outd(IOADDRESS + 0x3E, 0x1);
}

uint8_t * getMacAddress() {

    uint8_t macAddress0 = inb(IOADDRESS);  
    uint8_t macAddress1 = inb(IOADDRESS + 1);
    uint8_t macAddress2 = inb(IOADDRESS + 2);
    uint8_t macAddress3 = inb(IOADDRESS + 3);
    uint8_t macAddress4 = inb(IOADDRESS + 4);
    uint8_t macAddress5 = inb(IOADDRESS + 5);

    rtl.macAddress[0]=macAddress0;
    rtl.macAddress[1]=macAddress1;
    rtl.macAddress[2]=macAddress2;
    rtl.macAddress[3]=macAddress3;
    rtl.macAddress[4]=macAddress4;
    rtl.macAddress[5]=macAddress5;

    return rtl.macAddress;
}

uint8_t * setUpRTL(){
    turnOnRTL();
    softwareReset();
    setImrIsr();
    configureReceiveBuffer();
    enableReceiveAndTransmitter();
    isrHandler();
    rtl.rxNum = 0;
    rtl.txNum = 0;
    return getMacAddress();
}

void sendMessage(uint16_t address, uint16_t str, uint16_t length) {
  uint16_t status = IOADDRESS + 0x10 + rtl.txNum * 0x04;
  uint16_t length;
  uint8_t * buf = rtl.txBuffer[rtl.txNum];

  // waits until the previous package is free.
  while (!(_in_port_32(status_address) & 0x2000)){}

  mem_cpy(&buf[0],data->mac_dest,MAC_ADDRESS_LENGTH);
  mem_cpy(&buf[MAC_ADDRESS_LENGTH],rtl_info.mac_addr,MAC_ADDRESS_LENGTH);
  mem_cpy(&buf[MAC_ADDRESS_LENGTH * 2],(uint8_t*)&(data->length),2);
  mem_cpy(&buf[MAC_ADDRESS_LENGTH * 2 + 2],data->data,data->length);
  _out_port_32(status_address,length);
  rtl_info.tx_num++;
  rtl_info.tx_num &= 0x03;
}


void
mem_cpy(uint8_t * buffer, uint8_t * cpy, uint64_t length){
  for ( int i = 0 ; i < length ; i++ ){
    buffer[i] = cpy[i];
  }
}