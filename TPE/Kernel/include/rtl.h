#include <naiveConsole.h>
#ifndef RTL_H_
#define RTL_H_

void turnOnRTL();
void softwareReset();
void initReceivedBuffer();
void setImrIsr();
void configureReceiveBuffer();
void enableReceiveAndTransmitter();
void isrHandler();
void setUpRTL();
uint8_t * getMacAddress();
void sendMessage(uint16_t address, uint16_t str, uint16_t length);

#endif /* RTL_H_ */
